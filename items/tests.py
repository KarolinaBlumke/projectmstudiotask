# -*- coding: utf-8 -*-
"""Test for models and views."""
from __future__ import unicode_literals

import json

from django.db import IntegrityError
from django.test import TestCase, RequestFactory

from .models import Item, Categories
from .views import ItemListView, ItemsByCategoryView


class ItemModelTest(TestCase):
    """Test Item model."""

    def test_string_representation(self):
        """Test string representation of model."""
        entry = Item(name='Teaspoon', code='TSP')
        self.assertEqual(str(entry), entry.name)

    def test_item_name_and_code_must_be_unique(self):
        """Test that name and code must be unique."""
        Item.objects.create(name='Teaspoon', code='TSP')
        with self.assertRaises(IntegrityError):
            Item.objects.create(name='Teaspoon', code='TSP')

    def test_item_name_must_be_unique(self):
        """Test that name must be unique."""
        Item.objects.create(name='Teaspoon', code='TSP')
        with self.assertRaises(IntegrityError):
            Item.objects.create(name='Teaspoon', code='TSN')

    def test_item_code_must_be_unique(self):
        """Test that code must be unique."""
        Item.objects.create(name='Teaspoon', code='TSP')
        with self.assertRaises(IntegrityError):
            Item.objects.create(name='Spoon', code='TSP')


class CategoriesModelTest(TestCase):
    """Test categories model."""

    def test_string_representation(self):
        """Test string representation of model."""
        entry = Categories(name='Cutlery')
        self.assertEqual(str(entry), entry.name)

    def test_category_name_must_be_unique(self):
        """Test that name must be unique."""
        Categories.objects.create(name='Cutlery')
        with self.assertRaises(IntegrityError):
            Categories.objects.create(name='Cutlery')

    def test_assaign_category_to_item(self):
        """Test that you can assign category to item."""
        category = Categories.objects.create(name='Cutlery')
        item = Item.objects.create(name='Teaspoon', code='TSP', category_id=1)
        self.assertEqual(category.id, item.category_id)


class ViewsTest(TestCase):
    """Test views."""

    def setUp(self):
        """Set up Request factory."""
        self.factory = RequestFactory()

    def prepare_data(self):
        """Prepare test data."""
        category = Categories(name='Cutlery')
        category.save()

        category2 = Categories(name='Appliance')
        category2.save()

        item = Item(name='Kettle', code='KET',
                    active=True, category_id=2)
        item.save()
        item2 = Item(name='Spoon', code='SPN',
                     active=False, category_id=1)
        item2.save()
        item3 = Item(name='Knife', code='KNF',
                     active=True, category_id=1)
        item3.save()

    def test_list_active_items_response(self):
        """Test that list of active items is returned."""
        self.prepare_data()

        request = self.factory.get('/active_items/list')
        response = ItemListView().get(request)
        # Only Two objects should be in response
        # Because their state is active
        resp = [
            {
                "active": True,
                "code": "KET",
                "id": 1,
                "name": "Kettle"
            },
            {
                "active": True,
                "code": "KNF",
                "id": 3,
                "name": "Knife"
            }
        ]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response._headers,
            {'content-type': ('Content-Type', 'application/json')})
        self.assertEqual(json.loads(response.content), resp)

    def test_list_of_categories(self):
        """Test that list of categories is returned."""
        self.prepare_data()

        request = self.factory.get('/active_items/by_category/')
        response = ItemsByCategoryView().get(request)
        resp = [
            {
                "id": 1,
                "name": "Cutlery"
            },
            {
                "id": 2,
                "name": "Appliance"
            }
        ]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response._headers,
            {'content-type': ('Content-Type', 'application/json')})
        self.assertEqual(json.loads(response.content), resp)

    def test_list_active_items_by_category_post(self):
        """Test list of active items within category is returned."""
        self.prepare_data()

        request = self.factory.post(
            '/active_items/by_category/', {"category": "Cutlery"}
        )
        response = ItemsByCategoryView().post(request)
        resp = [
            {
                "id": 3,
                "name": "Knife"
            }
        ]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response._headers,
            {'content-type': ('Content-Type', 'application/json')})
        self.assertEqual(json.loads(response.content), resp)
