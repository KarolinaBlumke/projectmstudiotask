"""Define urls."""
from django.conf.urls import url
from .views import ItemListView, ItemsByCategoryView

urlpatterns = [
    url(r'^list/', ItemListView.as_view()),
    url(r'^by_category/', ItemsByCategoryView.as_view()),
]
