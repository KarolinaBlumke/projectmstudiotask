# -*- coding: utf-8 -*-
"""API endpoint views."""
from __future__ import unicode_literals

import json

from django.http import HttpResponse
from django.views.generic.list import View

from .models import Item, Categories


class ItemListView(View):
    """Return list of active items in JSON."""

    def get(self, request):
        """Return respond to GET request."""
        # Filter active objects
        queryset = Item.objects.filter(active=True)
        data = json.dumps(
            [
                {
                    "id": i.id,
                    "code": i.code,
                    "name": i.name,
                    "active": i.active
                } for i in queryset],
            indent=4
        )
        return HttpResponse(
            data, content_type='application/json'
        )


class ItemsByCategoryView(View):
    """Define GET and POST method for endpoint."""

    def get(self, request):
        """Return list of categories."""
        queryset = Categories.objects.all()
        data = json.dumps(
            [{"id": i.id, "name": i.name} for i in queryset],
            indent=4
        )
        return HttpResponse(data, content_type='application/json')

    def post(self, request):
        """Return a list of active items within catagory."""
        items = Item.objects.filter(
            category__name=request.POST['category'],
            active=True
        )
        data = json.dumps(
            [{"id": i.id, "name": i.name} for i in items],
            indent=4
        )
        return HttpResponse(
            data, content_type='application/json'
        )
