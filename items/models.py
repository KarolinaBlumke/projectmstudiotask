# -*- coding: utf-8 -*-
"""Models."""
from __future__ import unicode_literals

from django.db import models


class Categories(models.Model):
    """Define Categories model."""

    name = models.CharField(max_length=100, blank=False,
                            null=False, unique=True)

    def __str__(self):
        """Return string representation of model."""
        return self.name


class Item(models.Model):
    """Define Item model."""

    code = models.CharField(max_length=3, blank=False,
                            null=False, unique=True)
    name = models.CharField(max_length=100, blank=False,
                            null=False, unique=True)
    active = models.BooleanField(default=True)
    category = models.ForeignKey(
        Categories, on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        """Return string representation of model."""
        return self.name
