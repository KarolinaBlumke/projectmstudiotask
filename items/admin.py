# -*- coding: utf-8 -*-
"""Register models in admin panel."""
from __future__ import unicode_literals

from django.contrib import admin
from .models import Item, Categories

admin.site.register(Item)
admin.site.register(Categories)
