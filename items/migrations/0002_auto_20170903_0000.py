# -*- coding: utf-8 -*-
"""Generated by Django 1.11.4 on 2017-09-03 00:00."""
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    """Define migrations."""

    dependencies = [
        ('items', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True,
                                        serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='item',
            name='category',
            field=models.ForeignKey(
                            null=True,
                            on_delete=django.db.models.deletion.CASCADE,
                            to='items.Categories'),
        ),
    ]
