# -*- coding: utf-8 -*-
"""App config."""
from __future__ import unicode_literals

from django.apps import AppConfig


class ItemsConfig(AppConfig):
    """Items app config."""

    name = 'items'
