# README #
* Author: Karolina Blumke
* Application name: projectmstudiotask
* Python version: 2.7

### This repository is my solution of Project M Studio recrutation task. ###

* It's a small Django app which allow user to call API endpoints
* [Link to repo](https://bitbucket.org/KarolinaBlumke/projectmstudiotask/overview)

### How do I get set up? ###

I. Copy the source code

    Clone git repository [repo](https://KarolinaBlumke@bitbucket.org/KarolinaBlumke/projectmstudiotask.git) by:
        * `$ git clone https://KarolinaBlumke@bitbucket.org/KarolinaBlumke/projectmstudiotask.git`

II. Environment preparation

    To run app you must prepare Virtual environment.

        a. To get Virtualenv:

            * To install globally with pip (if you have pip 1.3 or greater installed globally):

                $ [sudo] pip install virtualenv

            * Or to get the latest unreleased dev version:

                $ [sudo] pip install https://github.com/pypa/virtualenv/tarball/develop

        b. create Virtualenv

            $ virtualenv ENV

            ENV - name for your virtualenv

        c. activate Virtualenv

            $ source ENV/bin/activate

            and go to the project folder: ../projectmstudiotask/

*** Instructions below require activate virtualenv!!! ***

III. Install requirements:

    Activate virtualenv, go to project folder (../projectmstudiotask/requirements.txt) and run:

        $ pip install -r requirements.txt

IV. Setting up database

    Go to project folder (../projectmstudiotask/manage.py) and run:

        $ python manage.py migrate

V. Run server/application:

    $ python manage.py runserver

    and go to the link given in the terminal (usually http://127.0.0.1:8000/)

VI. Run automatic tests

    To run tests go to project folder (../projectmstudiotask/manage.py) and run:

        $ python manage.py test items


VII. Usage

        a. Load data from database dump

            I also provided a dump of my database, so you can load my set of example data directly to your database.

            Make sure that you run:
                $ python manage.py migrate
            Before you run:
                $ python manage.py loaddata db.json

            Then you can login as admin/test1234 in the admin panel.

        b. Play with endpoints

            To run app you must run server:

            $ python manage.py runserver

            You have 3 endpoints to play with, and you can use you webbrowser (to GET endpoints), curl or Postman app [link](https://www.getpostman.com/).

            GET endpoints:

            * `/active_items/list/` - return list of all active items
            * `/active_items/by_category/` - return list of defined categories

            POST endpoints:
            * '/active_items/by_category/' - return list of active items within given category

        c. Examples using curl:

            $ curl -v -X POST --data "category=Cutlery" http://127.0.0.1:8000/active_items/by_category/

            Return:
                < HTTP/1.0 200 OK
                < Date: Sun, 03 Sep 2017 12:33:34 GMT
                < Server: WSGIServer/0.1 Python/2.7.13
                < X-Frame-Options: SAMEORIGIN
                < Content-Type: application/json
                < Content-Length: 56
                <
                [
                    {
                        "id": 3,
                        "name": "Fork"
                    }
                ]


            $ curl -v http://127.0.0.1:8000/active_items/list/

            Return:
                < HTTP/1.0 200 OK
                < Date: Sun, 03 Sep 2017 12:35:30 GMT
                < Server: WSGIServer/0.1 Python/2.7.13
                < X-Frame-Options: SAMEORIGIN
                < Content-Type: application/json
                < Content-Length: 211
                <
                [
                    {
                        "active": true,
                        "code": "KET",
                        "id": 1,
                        "name": "Kettle"
                    },
                    {
                        "active": true,
                        "code": "FRK",
                        "id": 3,
                        "name": "Fork"
                    }
                ]

VIII. Issues

If you have any issues with my application, please contact me:

* Karolina Blumke `karolina.blumke@gmail.com` [e-mail](karolina.blumke@gmail.com)
